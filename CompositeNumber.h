#pragma once
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <algorithm>

void print_vector(const std::vector<std::size_t>& vec);
void composite_number(std::vector<std::size_t>& vec);

class Number
{
public:
	Number(std::size_t origin_value, std::uint64_t new_value) :
		origin_value_(origin_value),
		new_value_(new_value)
	{};
	~Number() {};

	std::size_t value() { return origin_value_; }

	bool operator > (const Number &rhs) const {
		return this->new_value_ > rhs.new_value_;
	}
	bool operator < (const Number &rhs) const {
		return this->new_value_ < rhs.new_value_;
	}

private:
	std::size_t origin_value_;
	std::uint64_t new_value_;
};
