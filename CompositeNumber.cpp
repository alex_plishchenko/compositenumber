#include "CompositeNumber.h"

void composite_number(std::vector<std::size_t>& vec)
{
	std::vector<Number> v_number;
	std::vector<std::size_t> result;

	//normalization numbers
	std::size_t max_number_len = 0;
	for (const auto &number : vec) {
		std::size_t len_number = std::to_string(number).length();
		if (max_number_len < len_number) {
			max_number_len = len_number;
		}
	}

	for (const auto &number : vec) {
		std::string str_number = std::to_string(number);
		std::size_t len_number = str_number.length();
		str_number.append(max_number_len - len_number, str_number[0]);
		v_number.push_back(Number(number, std::stoull(str_number)));
	}
	//---

	std::sort(v_number.begin(), v_number.end(), std::greater<>());

	for (auto &number : v_number) {
		result.push_back(number.value());
	}

	vec = result;
}

void print_vector(const std::vector<std::size_t>& vec)
{
	for (auto &number : vec) {
		std::cout << number << " ";
	}
	std::cout << std::endl;
}