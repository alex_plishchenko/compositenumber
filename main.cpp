//Напишите функцию, которая принимает список не отрицательных целых чисел,
//упорядочивает их так, чтобы они составляли максимально возможное число.
//Например, [50, 2, 1, 9], наибольшее сформированное число равно 95021

#include <iostream>

#include "CompositeNumber.h"
#include "test.h"

int main()
{
	std::vector<std::size_t> in_array = { 50, 2, 1, 9 };
	composite_number(in_array);

	print_vector(in_array);

	//run_test();
}