#pragma once
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <algorithm>

void TEST_CHECK(std::vector<std::size_t> vec_1, std::vector<std::size_t> vec_2, std::size_t index_test) {
	bool isEqual = std::equal(std::begin(vec_1), std::end(vec_1), std::begin(vec_2));
	std::string str_result = (isEqual)?("OK"):("FAILED");
	std::string sufix = "test. n-line: " + std::to_string(index_test);
	std::cout<<"["<<std::setw(6)<< str_result << "]   "<< sufix << std::endl;
}

void run_test()
{
	{
		std::vector<std::size_t> array = { 50, 2, 1, 9 };
		std::vector<std::size_t> test_array = { 9, 50, 2, 1 };
		composite_number(array);
		TEST_CHECK(array, test_array, __LINE__);
	}
	
	{
		std::vector<std::size_t> array = { 9, 50, 22, 224 };
		std::vector<std::size_t> test_array = { 9, 50, 224, 22 };
		composite_number(array);
		TEST_CHECK(array, test_array, __LINE__);
	}

	{
		std::vector<std::size_t> array = { 1, 50, 22, 224 };
		std::vector<std::size_t> test_array = { 9, 50, 224, 22 };
		composite_number(array);
		TEST_CHECK(array, test_array, __LINE__);
	}
}

